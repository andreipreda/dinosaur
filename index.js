require('dotenv-json')();
require('./auth/models/db');

const app = require('./dinosaur');
const port = process.env.APP_PORT || 8080;

app.listen(port, () => console.log('Dinosaur started!'));
