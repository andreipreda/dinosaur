/**
 * Using magical data-forge parses kind of any reasonable CSV into chart data
 * Copyright Andrei Preda @Barbarosa Software Ltd
 */
const dataForge = require('data-forge')

const transformFn = (value) => Number(value)
const rand = (arr) => arr[Math.floor(Math.random() * arr.length)]

/**
 * Processes CSV data into chartData by random pivoting - simulation data processing step
 * @param {csv table like json data} data
 * @type any[][]
 * @example [["COL1", "COL2"], [1, 3], [2, 3]]
 * @returns { "content": [{ "labels": ['COL1', "COL2", "sum | average"], "data": [[COL1:1], [COL1: 2]] }] }
 */
const processor = (data) => {
  try {
    const columnNames = data[0];
    const skipHeader = data.shift();
    const testTypes = data[0];
    const rows = data;
    const index = [...Array(rows.length).keys()]

    // creates the initial dataframe with all string values
    const baked = new dataForge.DataFrame({
      columnNames,
      rows,
      index
    })

    // test and convert floats ints if the case to be able to pivot by them
    const testResults = testTypes.map(Number)
    // drop any ID col as we already index by numbers
    const numberColumns = columnNames.filter((name, idx) => Boolean(testResults[idx]) && name.toUpperCase() !== 'ID' )
    const stringColumns = columnNames.filter((_, idx) => !Boolean(testResults[idx]))
    const pivotFns = [val => val.sum(), val => val.average()]
    const fnLabels = ['sum', 'average']

    const transformations = {}
    numberColumns.forEach(name => transformations[name] = transformFn)

    // transform string values with the tested ones
    const normalizedDf = baked.transformSeries(transformations)

    // generating random pivoting data
    return Array(4)
      .fill({})
      .map(_ => {
        const randBool = Math.round(Math.random())
        const genDf = normalizedDf.pivot([rand(stringColumns)], rand(numberColumns), pivotFns[randBool])
        const labels = genDf.getColumnNames();
        labels.push(fnLabels[randBool])

        return { labels, data: genDf.toRows() }
      })
  } catch (err) {
    throw new Error('Cannot process your csv file. Try to use a table like structure');
  }
}

module.exports = processor;
