const mongoose = require('mongoose');

const csvSchema = new mongoose.Schema({
  content: {
    type: {},
    required: true
  },
  created_at: Date,
  name: String,
  user_id: {
    type: mongoose.Schema.Types.ObjectId, ref: 'User',
    required: true
  }
}, {strict: true});

mongoose.model('CsvSchema', csvSchema);
