const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const authorization = require('./../auth/controllers/authorization');
const Papa = require('papaparse');
const multer = require('multer');
// const azureStorage = require('azure-storage');
// const blobService = azureStorage.createBlobService(process.env.BLOB_CON);
const getStream = require('into-stream');
const processor = require('./processor');

const router = express.Router();
const inMemoryStorage = multer.memoryStorage();
const CsvSchema = mongoose.model('CsvSchema');

// Promisify papaparse
Papa.parsePromise = (stream) => new Promise((complete, error) => {
  Papa.parse(stream, {complete, error});
});

const getBlobName = originalName => {
  const identifier = Math.random().toString().replace(/0\./, ''); // remove "0." from start of string
  return `${identifier}-${originalName}`;
};

const uploadStrategy = multer({
  storage: inMemoryStorage
}).single('csvfile');

router.use(bodyParser.json());
router.use(authorization.tokenCheck);

router.post('/add', uploadStrategy, async (req, res) => {
  const { user: { _id, name } } = req;
  const blobName = getBlobName(req.file.originalname)

  if (path.extname(req.file.originalname) !== '.csv') {
    return res.status(400).json({ error: 'Only csv files are allowed!' })
  }
  const stream = getStream(req.file.buffer)
  const streamLength = req.file.buffer.length;

  try {
    const { data } = await Papa.parsePromise(stream);

    const dataStruc = new CsvSchema({
      content: processor(data),
      created_at: new Date(),
      name: blobName,
      user_id: _id
    });

    dataStruc.save((err, db_data) => {
      if (err) {
        console.error(err)
        return res.status(400).json({error: 'DB error saving data!'})
      }
      console.log(`New data for ${name} added`);
      const { content, _id, created_at } = db_data;
      res.status(201).json({ content, name, _id, created_at });
    })

  } catch(e) {
    return res.status(500).json({error: e.message})
  }
});

router.get('/all', (req, res) => {
  const { user: { _id } } = req;
  const query = CsvSchema
    .find({ user_id: _id })
    .select({ name: true, _id: true, created_at: true })
    .sort({ created_at: 'desc' });

  query.exec((err, results) => {
    if (err) {
      console.error(err)
      return res.status(400).json({error: 'Mongo error fetching data'});
    }
    res.status(200).json(results);
  })
});

router.get('/csv/:id', (req, res) => {
  if (!req.query.id) {
    return res.status(400).json({error: 'An id must be passed in the query'});
  }
  CsvSchema.findById(req.query.id, (err, db_doc) => {
    if (err) {
      console.error(err)
      return res.status(400).json({error: 'Mongo error fetching data'});
    }
    res.status(200).json(db_doc);
  });
});

router.delete('/csv/:id', (req, res) => {
  if (!req.query.id) {
    return res.status(400).json({error: 'An id must be passed in the query'});
  }
  CsvSchema.deleteOne({ _id: req.query.id }, (err) => {
    if (err) {
      console.error(err)
      return res.status(400).json({error: 'Mongo error deleting data'});
    }
    res.status(200).json({deleted: 'ok'});
  });
});


// router.post('/upload', uploadStrategy, (req, res) => {

//   const { user: { _id } } = req;
//   const blobName = getBlobName(req.file.originalname)
//   const stream = getStream(req.file.buffer)
//   const streamLength = req.file.buffer.length;

//   blobService.createBlockBlobFromStream(
//     containerName,
//     blobName,
//     stream,
//     streamLength,
//     { metadata: {
//       user_id: _id
//     }},
//     err => {
//     if (err) {
//       console.log(err)
//       return res.status(500).json({ error: 'Your uploaded file cannot be stored' })
//     }
//     return res.status(200).json({ message: 'File uploaded to Azure Blob storage.'});
//   });
// });

module.exports = router;
