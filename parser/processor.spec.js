const processor = require('./processor');

const input_data = [
  ["county","state","area","pop","p18_25","p65","nphysician","nhospbeds","phs","pcollege","ppoverty","punemployed","avg_income","tot_income","region","crime"],
  ["Los_Angeles","CA",4060,8863164,32.1,9.7,23677,27700,70,22.3,11.6,8,20786,184230,4,77.7302552451924],
  ["Cook","IL",946,5105067,29.2,12.4,15153,21550,73.4,22.8,11.1,7.2,21729,110928,2,85.5886906087618],
  ["Harris","TX",1729,2818199,31.3,7.1,7553,12449,74.9,25.4,12.5,5.7,19517,55003,3,89.9602902421014]
]

describe('processor', () => {
  test('should output correct', () => {
    const output = processor(input_data);
    expect(output).toBeInstanceOf(Array);
    expect(output.length).toBe(4);
    expect(output[0]).toHaveProperty('labels');
    expect(output[0]).toHaveProperty('data');
  })
});