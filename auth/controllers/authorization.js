const mongoose = require('mongoose');
const User = mongoose.model('User');
const bearerRx = /^Bearer /;

/**
 * Custom middleware to validate the token and possible send a user forward
 */
module.exports.tokenCheck = async (req, res, next) => {

  if (!req.headers.authorization) {
    return res.status(401).json({'error' : 'UnauthorizedError: You need a token to access this resource'})
    // next(null, false);
  } else {
    const user = new User();
    try {
      // check bearer keyword and possible remove it
      const possibleToken = bearerRx.test(req.headers.authorization)
        ? req.headers.authorization.slice(7)
        : req.headers.authorization;

      const received_user = await user.verifyJwt(possibleToken)
      if (received_user) {
        req.user = received_user
        next()
      } else {
        return res.status(401).json({'error' : 'UnauthorizedError: Invalid user'})
      }
    } catch (e) {
      console.error(e)
      return res.status(401).json({'error' : 'UnauthorizedError: Invalid token'})
      // next(null, false)
    }
  }
};
