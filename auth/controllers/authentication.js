const passport = require('passport');
const mongoose = require('mongoose');
const User = mongoose.model('User');

module.exports.register = (req, res) => {

  if(!req.body.name || !req.body.email || !req.body.password) {
    return res.status(400).json({'error': 'All fields required' });
  }

  const user = new User();

  user.name = req.body.name;
  user.email = req.body.email;
  user.hash = user.setPassword(req.body.password);

  user.save((err) => {
    if (err) {
      if (err.name === 'MongoError' && err.code === 11000) {
        return res.status(409).json({error: 'An user with this email already exists'});
      }
      else {
        console.error(err)
        return res.status(500).json({error: 'DB Error'});
      }
    } else {
      user.generateJwt(user._id, user.email, user.name).then(token => {
        return res.status(201).json({ token, _id: user._id, email: user.email, name: user.name });
      })
    }
  });

};

module.exports.login = (req, res) => {

  if (!req.body.email || !req.body.password) {
    return res.status(400).json({'error': 'All fields required' })
  }

  passport.authenticate('local', (err, user, info) => {

    // If passport throws/catches an error
    if (err) {
      return res.status(404).json(err);
    }

    // If a user is found
    if (user) {
      user.generateJwt(user._id, user.email, user.name).then(token => {
        return res.status(200).json({ token, _id: user._id, email: user.email, name: user.name });
      })

    } else {
      // If user is not found
      return res.status(401).json(info);
    }
  })(req, res);

};
