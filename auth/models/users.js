const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const jwt = require('jsonwebtoken');

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    unique: true,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  hash: String
}, {strict: true});


userSchema.methods.setPassword = (password) => {
  try {
    return bcrypt.hashSync(password)
  } catch (e) {
    console.error('Error hash password')
  }
};

userSchema.methods.validPassword = (db_password, hash) => {
  try {
    return bcrypt.compareSync(db_password, hash);
  } catch(e) {
    console.error('Error comparing hashed passwords', e)
  }
};

userSchema.methods.generateJwt = async (_id, email, name) => {
  const expiry = new Date();
  expiry.setDate(expiry.getDate() + 21);

  try {
    return await jwt.sign({
      _id: _id,
      email: email,
      name: name,
      exp: parseInt(expiry.getTime() / 1000),
    }, process.env.SECRET); 
  } catch (e) {
    console.error('JWT Signing', e.message);
  }
};

userSchema.methods.verifyJwt = async (token) => {
  return await jwt.decode(token, process.env.SECRET)
}

mongoose.model('User', userSchema);
