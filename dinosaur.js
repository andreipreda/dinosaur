require('./auth/models/users');
require('./parser/model');
require('./auth/config/passport');

const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const passport = require('passport');
const authRoutes = require('./auth/routes/index');
const parserRoutes = require('./parser/index');

const app = express();

app.use(bodyParser.json({ extended: true }));
app.use(passport.initialize());
app.use(cors());

app.use('/auth', authRoutes);
app.use('/api', parserRoutes);

app.get('/', (req, res) => {
  res.send('Dinosaur Express Azure CSV Parser API')
});

module.exports = app
