const request = require('supertest');
const app = require('./dinosaur');
const mongoose = require('mongoose');
const Mockgoose = require('mockgoose').Mockgoose;

// set for the in-memory mongo mock to ensure it has enough time to initialise.
const timeout = 120000;
// data used to pass the auth for accessing user data
let token = null;
let doc_id = null;

beforeAll(() => {
  process.env.SECRET = 'JURASSIC_AGE'
  // mock mongo with an in-memory mongo like db thanks to mockgoose!
  const mockgoose = new Mockgoose(mongoose);
  mockgoose.prepareStorage().then(() => {
    mongoose.connect('mongodb://dinosaurs/on_earth');
    mongoose.connection.on('connected', () => {
      console.log('test db connection is now open');
    }); 
  });
});

describe('Test the root path', () => {
  test('It should response the GET method', async () => {
      const response = await request(app).get('/');
      expect(response.statusCode).toBe(200);
  });
});

describe('Auth routes', () => {

  test('Should register a new account', async () => {
    const response = await request(app)
      .post('/auth/register')
      .send({ email: 'john@x.y', name: 'john', password: '123' })

    expect(response.status).toEqual(201);
    expect(response.body).toBeDefined()
    const keys = Object.keys(response.body).sort();
    expect(keys).toEqual(['token', '_id', 'email', 'name'].sort());
  }, timeout);


  test('Should login', async () => {
    const response = await request(app)
      .post('/auth/login')
      .send({ email: 'john@x.y', password: '123' })

    expect(response.status).toEqual(200);
    expect(response.body).toBeDefined();
    expect(response.body.token).toBeDefined();
    // set token to be used later run in order tests!
    token = response.body.token;
    const keys = Object.keys(response.body).sort();
    expect(keys).toEqual(['token', '_id', 'email', 'name'].sort());
  }, timeout);


  test('Should not login with the wrong password!', async () => {
    const response = await request(app)
      .post('/auth/login')
      .send({ email: 'john@x.y', password: 'wrong' })

    expect(response.status).toEqual(401);
    expect(response.body).toHaveProperty('error');
  }, timeout)
});

describe('Parser routes', () => {

  test('Should be able to upload a file', async () => {
    const response = await request(app)
      .post('/api/add')
      .set('Authorization', token)
      .attach('csvfile', 'test_data.csv')

    expect(response.status).toEqual(201);
    const keys = Object.keys(response.body).sort();
    expect(keys).toEqual(['content', '_id', 'name', 'created_at'].sort());
    doc_id = response.body._id;
    // expecting 4 processed chart data
    expect(response.body.content).toHaveLength(4)
  }, timeout)


  test('Should retrieve all existing parsed docs', async () => {
    const response = await request(app)
      .get('/api/all')
      .set('Authorization', token)
      .send()

    expect(response.status).toEqual(200);
    expect(response.body).toHaveLength(1)
  }, timeout)


  test('Should retrieve detailed doc requested', async () => {
    const response = await request(app)
      .get('/api/csv/&')
      .query({'id': doc_id})
      .set('Authorization', token)
      .send()

    expect(response.status).toEqual(200);
    expect(response.body.content).toBeDefined();
  }, timeout)


  test('Should remove an existing doc', async () => {
    const response = await request(app)
      .delete('/api/csv/&')
      .query({'id': doc_id})
      .set('Authorization', token)
      .send()

    expect(response.status).toEqual(200);
    expect(response.body).toHaveProperty('deleted', 'ok');
  }, timeout)

});
