# Dinosaur

Express - Node API to upload/parse and CRUD (without update) on a CSV file and also handles Authentication and Authorization.
All persisted in a MongoDB.

**[How to use docs](https://documenter.getpostman.com/view/4289/RWMBRAfx)** - connects to a local instance

## Run locally 

Run `npm install` or `yarn` if the case for dependencies.

Create a .env.json file in the root dir with the following values:

{
  "DB_CON": "...MongoDB connection URL here...",
  "SECRET": "...a secret used by JWT to has the passwords..."
}

To start run `yarn nodemon` for watching the files or just `node index.js` for a simple start.

## Run Unit Test

It uses [Jest](https://jestjs.io/) as a test framework

Run `npm test` to execute the unit tests. or `yarn test` or `yarn jest`.

It in using an in-memory mongodb mock to mock the real db called [mockgoose](https://github.com/mockgoose/mockgoose)
